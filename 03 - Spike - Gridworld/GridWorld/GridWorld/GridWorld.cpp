// GridWorld.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

const int MapSizeX = 8;
const int MapSizeY = 8;

class Map
{
public:
	char data[MapSizeX][MapSizeY] = {
		{ '#', '#', '#', '#', '#', '#', '#', '#' },
		{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
		{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
		{ '#', '#', 'P', '#', '#', '#', '#', '#' } };
};

struct Input
{
	int deltaPosX = 0;
	int deltaPosY = 0;
	int special = -1; //Set to '0' to quit
};

///<summary>
///Loop through the given 'map' (2D array of chars) and print each character to the screen,
///following each X point with a space, and each Y line with a new line.
///</summary> 
void DrawMap(Map* map) {//char Map[MapSizeY][MapSizeX]) {
	for (int j = 0; j < MapSizeY; j++) {
		for (int k = 0; k < MapSizeX; k++) {
			cout << map->data[j][k] << ' ';
		}
		cout << endl;
	}
}

///<summary>
///Wait for a string input, interpret a direction (like 'n' into dy = -1) or action (like 'q' into quit) from it,
///and then return the interpreted input.
///</summary> 
Input GetInput()
{
	//   N
	// W   E
	//   S

	cout << "Please enter a direction (N/S/E/W), or Q to quit: ";

	Input result;
	string textInput;
	getline(cin, textInput);

	transform(textInput.begin(), textInput.end(), textInput.begin(), ::toupper);

	if (textInput == "Q") {
		result.special = 0;
	}
	else if (textInput == "W")
	{
		result.deltaPosY = -1;
	}
	else if (textInput == "E")
	{
		result.deltaPosY = 1;
	}
	else if (textInput == "N")
	{
		result.deltaPosX = -1;
	}
	else if (textInput == "S")
	{
		result.deltaPosX = 1;
	}

	return result;
}

///<summary>
///Take the user input directly, and the player position and map as pointers,
///and use the input to update the game state. Returns true if the game should end.
///</summary> 
bool Update(Input userIn, int* playerX, int* playerY, Map* map)
{
	if (userIn.special == 0)
	{
		cout << "Sad to see you go :(" << endl;
		return true; //Special '0' is the 'Quit' action.
	}

	int newPosX = *playerX + userIn.deltaPosX;
	int newPosY = *playerY + userIn.deltaPosY;

	if ((newPosX >= 0 && newPosX < MapSizeX) && (newPosY >= 0 && newPosY < MapSizeY)) {
		bool done = false;
		if (map->data[newPosX][newPosY] != '#') {

			map->data[*playerX][*playerY] = ' ';

			if (map->data[newPosX][newPosY] == 'G') {
				cout << endl << endl << "You Win! :)" << endl;
				done = true;
			}
			else if (map->data[newPosX][newPosY] == 'D') {
				cout << endl << endl << "You Died! :(" << endl;
				done = true;
			}

			map->data[newPosX][newPosY] = 'P';

			*playerX = newPosX;
			*playerY = newPosY;

			return done;
		}
		else {
			cout << "You can't move there. :/" << endl;
		}
	}
	else {
		cout << "Please don't go outside the map. That would break the game." << endl;
	}
	return false;
}

int main()
{
	Map* map = new Map();
	int playerX = 0;
	int playerY = 0;

	//Find the start position, set the players coordinates.
	for (int j = 0; j < MapSizeX; j++) {
		for (int k = 0; k < MapSizeY; k++) {
			if (map->data[j][k] == 'P') {
				playerX = j;
				playerY = k;
			}
		}
	}

	bool done = false;
	Input userIn;
	int newPosX;
	int newPosY;

	cout << "WELCOME TO GRIDWORLD!" << endl << endl;
	cout << "Compass:" << endl << "   N" << endl << " W   E" << endl << "   S" << endl << endl;
	while (done == false) {
		DrawMap(map);
		userIn = GetInput();
		done = Update(userIn, &playerX, &playerY, map);

		if (done)
			system("pause");
	}

	return 0;
}