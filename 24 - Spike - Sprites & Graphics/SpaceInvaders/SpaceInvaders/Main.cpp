#include <iostream>
#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <string>
#include <map>

using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

struct Sprite {
	SDL_Rect rect;
	SDL_Texture* image = nullptr;
};

void Input(SDL_Event& e);
void Render();
bool Init();                                                // Starts up SDL and creates window
bool LoadMedia();                                           // Loads media
void CloseSDL();                                               // Frees media and shuts down SDL

SDL_Window* gWindow = nullptr;                              // Window to render to
SDL_Renderer* gRenderer = nullptr;							// Renderer
map<int, bool> gToggles;
Sprite gBackground;
Sprite gLeft;
Sprite gIdle;
Sprite gRight;
bool quit = false;											// Main loop flag

int main(int argc, char* args[])
{
	cout << "Window: " << gWindow << " Renderer: " << gRenderer << endl;
	SDL_Event e;        // Event handler
	if (Init() && LoadMedia()) {

		// Main game loop
		while (!quit) {
			cout << "Window: " << gWindow << " Renderer: " << gRenderer << endl;
			Input(e);
			Render();
		}
	}

	CloseSDL(); // Free resources and close SDL

	return 0;
}

void Input(SDL_Event& e) {
	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT)
			quit = true;
		else if (e.type == SDL_KEYDOWN)
		{
			//Toggle images based on key press
			switch (e.key.keysym.sym)
			{
			case SDLK_1:
				gToggles[1] = !gToggles[1];
				break;
			case SDLK_2:
				gToggles[2] = !gToggles[2];
				break;
			case SDLK_3:
				gToggles[3] = !gToggles[3];
				break;
			case SDLK_0:
				gToggles[0] = !gToggles[0];
				break;
			}
		}
	}
}

//Render an the image using the position and dimensions of the given rect.
void RenderScaledImage(Sprite sprite, SDL_Rect* ScreenRect) {
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), ScreenRect);
}

//Render an the image using x and y screen positions, and its default dimensions. (x=0=left, y=0=top)
void RenderImageAt(Sprite sprite, int x, int y) {
	SDL_Rect pos = { x, y, sprite.rect.w, sprite.rect.h };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render an the image using x and y screen positions, and custom dimensions. (x=0=left, y=0=top)
void RenderScaledImageAt(Sprite sprite, int x, int y, int w, int h) {
	SDL_Rect pos = { x, y, w, h };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render an the image using the position given rect, and its default dimensions.
void RenderImage(Sprite sprite, SDL_Rect* ScreenPos) {
	SDL_Rect pos = *ScreenPos;
	pos.w = sprite.rect.w;
	pos.h = sprite.rect.h;
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

void RenderBackground(Sprite sprite) {
	SDL_RenderCopy(gRenderer, sprite.image, NULL, NULL);
}

void Render() {

	//Clear screen
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(gRenderer);

	if (gToggles[0])
		RenderBackground(gBackground);
	if (gToggles[1])
		RenderScaledImageAt(gLeft, 470, 300, 128, 128);
	if (gToggles[2])
		RenderScaledImageAt(gIdle, 250, 300, 128, 128);
	if (gToggles[3])
		RenderScaledImageAt(gRight, 20, 300, 128, 128);

	//Update screen
	SDL_RenderPresent(gRenderer);
}

bool Init()
{
	// Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL was not initialized. SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	// Create window
	gWindow = SDL_CreateWindow("Sprite Testing", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (gWindow == nullptr) {
		printf("Window was not created. SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	// Create Renderer for window
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if (gRenderer == nullptr) {
		printf("Renderer was not created. SDL Error: %s", SDL_GetError());
		return false;
	}

	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

	//Initialize PNG loading
	if (IMG_Init(IMG_INIT_PNG) == 0) {
		printf("SDL_image was not initialized. SDL_image Error: %s\n", IMG_GetError());
		return false;
	}

	gToggles[0] = false;
	gToggles[1] = false;
	gToggles[2] = false;
	gToggles[3] = false;

}

//Create a texture by saving the pixels loaded onto a temporary surface.
void LoadSprite(string path, Sprite* sprite) {
	SDL_Surface* tempSurface = IMG_Load(path.c_str());

	if (tempSurface != nullptr) {
		//Create texture from surface pixels
		sprite->image = SDL_CreateTextureFromSurface(gRenderer, tempSurface);
		if (sprite->image != nullptr) {
			//Set the default sprite to 'cut' from the sheet to the size of the sprite.
			sprite->rect.w = tempSurface->w;
			sprite->rect.h = tempSurface->h;
		}
		else
			printf("Unable to create texture from %s. SDL Error: %s\n", path.c_str(), SDL_GetError());
		SDL_FreeSurface(tempSurface);
	}
	else
		printf("Unable to load image %s. SDL_image Error: %s\n", path.c_str(), IMG_GetError());
}

bool LoadMedia()
{
	LoadSprite("../media/Background.png", &gBackground);
	LoadSprite("../media/CharSheet.png", &gLeft);

	if (gBackground.image == nullptr || gLeft.image == nullptr)
		return false;

	gLeft.rect.w = 32;
	gIdle = gLeft;
	gIdle.rect.x = 32;
	gRight = gLeft;
	gRight.rect.x = 64;
	
	return true;
}

void CloseSDL()
{
	//Destroy window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = nullptr;
	gRenderer = nullptr;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}