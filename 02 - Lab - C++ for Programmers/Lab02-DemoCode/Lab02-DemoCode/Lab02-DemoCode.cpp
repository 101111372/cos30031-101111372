#include <iostream>
#include <cstdlib> 
#include <ctime>
#include <string>
#include <vector>
using namespace std;

class Mean
{
public:
	int count = 0;
	int GetAverage()
	{
		if (count > 0)
			return total / count;
		else
			return 0;
	}

	void AddValue(int value)
	{
		total += value;
		count += 1;
	}

private:
	int total = 0;
};

void printTwice(string value1, string value2)
{
	cout << "Value 1 is: " << value1 << endl;
	cout << "Value 2 is: " << value2 << endl;
}

int addition(int val)
{
	int result;
	result = val + 1;
	return result;
}

void secondEnderChest(int * enderChestPointer)
{
	*enderChestPointer = 100;
	cout << "Even ones in functions! (" << *enderChestPointer << ")" << endl;
}

void printOddNumbers(int upTo)
{
	cout << "Odd numbers up to " << upTo << ": ";
	for (int i = 1; i < upTo; i += 2)
	{
		cout << i << " ";
	}
	cout << endl;
}

void fiveRandomInts()
{
	int fiveInts[5];

	
	srand((unsigned)time(0));
	//srand();
	fiveInts[0] = rand();
	fiveInts[1] = rand();
	fiveInts[2] = rand();
	fiveInts[3] = rand();
	fiveInts[4] = rand();

	for (int i = 0; i < 5; ++i)
	{
		cout << "int[" << i << "] = " << fiveInts[i] << endl;
	}
}

void splitStringAndPrint(string input, char delimiter)
{
	//string input = "This text has many spaces in it.";
	vector<string> words;

	int lastDelimiter = 0;
	for (int i = 0; i <= input.length(); ++i)
	{
		if (input[i] == delimiter || input[i] == NULL) {
			words.push_back(input.substr(lastDelimiter, i - lastDelimiter));
			lastDelimiter = i + 1;
		}
	}

	cout << "Splitting the string:" << endl;
	for (int i = 0; i < words.size(); ++i)
	{
		cout << words[i] << endl;
	}
}

int main()
{
	cout << "G'day Universe" << endl;
	cout << endl;

	string one = "Evo";
	string two = " >= Tien?";
	printTwice(one, two);
	cout << endl;

	int z = 5;
	cout << "Starting with " << z << " and adding 1:" << endl;
	z = addition(5);
	cout << "The new value is " << z << endl;
	cout << endl;

	int enderChestContents;
	int * enderChest;
	enderChest = &enderChestContents;
	*enderChest = 10;
	cout << "Your items (" << *enderChest << ") can be accessed from any EnderChest!" << endl;
	secondEnderChest(&enderChestContents);
	cout << endl;

	printOddNumbers(20);
	cout << endl;

	fiveRandomInts();
	cout << endl;

	string unsplitText;
	cout << "Type out some text to split: ";
	getline(cin, unsplitText);
	cout << endl;
	splitStringAndPrint(unsplitText, ' ');
	cout << endl;

	cout << "Random Values:" << endl;
	Mean average;
	int randVal;
	randVal = rand();
	average.AddValue(randVal);
	cout << randVal << endl;
	randVal = rand();
	average.AddValue(randVal);
	cout << randVal << endl;
	randVal = rand();
	average.AddValue(randVal);
	cout << randVal << endl;
	randVal = rand();
	average.AddValue(randVal);
	cout << randVal << endl;
	randVal = rand();
	average.AddValue(randVal);
	cout << randVal << endl;

	cout << "Average of values is: " << average.GetAverage() << endl;

	cout << endl;
	system("pause");
	return 0;
}