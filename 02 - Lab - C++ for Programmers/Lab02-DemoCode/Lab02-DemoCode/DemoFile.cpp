using std;

#include <iostream>

void printTwice(char[] value1, char[] value2)
{
	cout << "Value 1 is: " << value1 << endl;
	cout << "Value 2 is: " << value2 << endl;
}

int main()
{
	cout << "G'day Universe" << endl;
	printTwice("Apples", "Oranges");
	system("pause");
	return 0;
}