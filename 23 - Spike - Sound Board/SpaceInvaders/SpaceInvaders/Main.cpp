#include "SDL.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <stdio.h>
#include <string>

//#include "pch.h"
#include <iostream>
//#include <SDL.h>
//#include <SDL_mixer.h>

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Key press surfaces constants
enum TestKeys
{
	KEY_DEFAULT,
	KEY_UP,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_TOTAL
};


bool init();                                                // Starts up SDL and creates window
bool loadMedia();                                           // Loads media
void close();                                               // Frees media and shuts down SDL
SDL_Surface* loadSurface(std::string path);					//Loads individual image
void toggleMusic();
void playSong(Mix_Music* mix);
bool testLoop();

SDL_Window* gWindow = nullptr;                              // Window to render to
SDL_Surface* gScreenSurface = nullptr;                      // Surface contained by window
SDL_Surface* gTestKeys[KEY_TOTAL];    // The image that correspond to a keypress
SDL_Surface* gCurrentSurface = nullptr;

//The music that will be played
Mix_Music* gCurrentMusic = nullptr;
Mix_Music* gBeat = nullptr;
Mix_Chunk* gHigh = NULL;
Mix_Chunk* gMedium = NULL;
Mix_Chunk* gLow = NULL;
Mix_Chunk* gScratch = NULL;


int main(int argc, char* args[])
{
	bool quit = false;  // Main loop flag
	gCurrentSurface = gTestKeys[KEY_DEFAULT]; // Set default current surface

	if (!(init() && loadMedia()))
		return 1;

	// Main game loop
	while (!quit) {
		quit = testLoop();
	}
	// Free resources and close SDL
	close();

	return 0;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		gWindow = SDL_CreateWindow("SDL Sound Board", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface(gWindow);
		}

		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}
	}

	return success;
}

bool loadMedia()
{
	bool success = true;

	gBeat = Mix_LoadMUS("../media/beat.wav");
	success = (success && gBeat != nullptr);
	gHigh = Mix_LoadWAV("../media/high.wav");
	success = (success && gHigh != nullptr);
	gMedium = Mix_LoadWAV("../media/medium.wav");
	success = (success && gMedium != nullptr);
	gLow = Mix_LoadWAV("../media/low.wav");
	success = (success && gLow != nullptr);
	gScratch = Mix_LoadWAV("../media/scratch.wav");
	success = (success && gScratch != nullptr);

	return success;
}

void close()
{
	// Destroy the music
	Mix_FreeMusic(gBeat);
	gBeat = nullptr;
	Mix_FreeChunk(gHigh);
	gHigh = nullptr;
	Mix_FreeChunk(gMedium);
	gMedium = nullptr;
	Mix_FreeChunk(gLow);
	gLow = nullptr;
	Mix_FreeChunk(gScratch);
	gScratch = nullptr;

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = nullptr;

	//Quit SDL subsystems
	SDL_Quit();
}

void toggleMusic()
{
	if (Mix_PlayingMusic() == 0)
		Mix_PlayMusic(gBeat, -1);
	else {
		if (Mix_PausedMusic() == 1) {
			Mix_ResumeMusic();
		}
		else {
			Mix_PauseMusic();
		}
	}
}

bool testLoop() {

	SDL_Event e;
	// Poll events
	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT)
		{
			return true; //Quit
		}
		else if (e.type == SDL_KEYDOWN)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_1:
				Mix_PlayChannel(-1, gHigh, 0);
				//Mix_PlayMusic(gMusic[MUSIC_FRIED_BEE], -1);
				break;
			case SDLK_2:
				Mix_PlayChannel(-1, gMedium, 0);
				break;
			case SDLK_3:
				Mix_PlayChannel(-1, gLow, 0);
				break;
			case SDLK_4:
				Mix_PlayChannel(-1, gScratch, 0);
				break;
			case SDLK_0:
				toggleMusic();
				break;
			}
		}
	}

	return false; //Don't quit.
}