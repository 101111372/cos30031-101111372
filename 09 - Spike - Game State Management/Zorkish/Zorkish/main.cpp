#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <algorithm>
#include <fstream>
#include <chrono> 
#include <ctime> 

using namespace std;

struct HighScore
{
	int score;
	string name;
	string world;
};

HighScore scores[11];

enum State
{
	StRepeat,
	StMenu,
	StSelect,
	StFameHall,
	StHelp,
	StAbout,
	StGameplay_Void,
	StGameplay_Water,
	StGameplay_Box,
	StNewScore,
	StQuit,
};

class GameState
{
protected:
	State thisState = StMenu;
	State nextState = StMenu;
	void SeperatorLine()
	{
		cout << "------------------------------" << endl << endl;
	}

public:
	virtual void Step() = 0;
	virtual State CheckTransition()
	{
		if (nextState != thisState)
			return nextState;
		else
			return StRepeat;
	}
	void Initialize(State state)
	{
		thisState = state;
		nextState = state;
	}
};

class Menu : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Main Menu" << endl;
		SeperatorLine();
		cout << "Welcome to Zorkish Adventures" << endl << endl;
		cout << "1. Select Adventure and Play" << endl;
		cout << "2. Hall of Fame" << endl;
		cout << "3. Help" << endl;
		cout << "4. About" << endl;
		cout << "5. Quit" << endl << endl;

		cout << "Select 1-5:> ";

		int choice = -1;
		cin >> choice;
		if (choice == 1)
			nextState = StSelect;
		else if (choice == 2)
			nextState = StFameHall;
		else if (choice == 3)
			nextState = StHelp;
		else if (choice == 4)
			nextState = StAbout;
		else if (choice == 5)
			nextState = StQuit;

		cout << endl << endl;
		cin.get();
	}
};

class About : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: About" << endl;
		SeperatorLine();
		cout << "Written by Jake Scott" << endl << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Help : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Help" << endl;
		SeperatorLine();
		cout << "The following commands are supported:" << endl;
		cout << "quit," << endl;
		cout << "hiscore (for testing)" << endl;
		/*
		cout << "[go] _, (or just n, ne, e, etc)" << endl;
		cout << "look at _," << endl;
		cout << "look in _," << endl;
		cout << "inventory," << endl;
		cout << "open _ [with _]," << endl;
		cout << "close _," << endl;
		cout << "attack _ with _," << endl;
		cout << "take _ [from _]," << endl;
		cout << "put _ in _," << endl;
		cout << "drop _," << endl;
		cout << "quit" << endl;
		cout << "[up arrow] to repeat last command" << endl;
		*/
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class NewScore : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: New High Score" << endl;
		SeperatorLine();
		cout << "Congratulations!" << endl << endl;

		cout << "You have made it to the Zorkish Hall of Fame" << endl << endl;

		string adventure = "Test";
		cout << "Adventure: " << adventure << endl;
		int score = 1000;
		cout << "Score: " << score << endl;
		int moves = 69;
		cout << "Moves: " << moves << endl;

		cout << "Please type your name and press Enter:" << endl;

		cout << ":> ";
		string scoreName = "Noob";
		cin >> scoreName;

		//>> Add score to highscore data << ??

		nextState = StFameHall;

		cout << endl << endl;
		cin.get();
	}
};

class FameHall : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Hall of Fame" << endl;
		SeperatorLine();
		cout << "Top 10 Zorkish Adventure Champions" << endl << endl;
		string name = "NoobMaster69";
		string world = "Planet World";
		int score = 5000;
		for (int i = 0; i < 10; ++i)
			cout << i << ". " << name << ", " << world << ", " << score << endl;
		cout << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Select : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Select Adventure" << endl;
		SeperatorLine();
		cout << "Choose your adventure:" << endl << endl;

		cout << "1. Void World" << endl;
		cout << "2. Water World" << endl;
		cout << "3. Box World" << endl;
		cout << "0. Cancel" << endl << endl;

		cout << "Select 0-3:> ";

		int choice = -1;
		cin >> choice;
		if (choice == 1)
			nextState = StGameplay_Void;
		else if (choice == 2)
			nextState = StGameplay_Water;
		else if (choice == 3)
			nextState = StGameplay_Box;
		else
			nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Gameplay : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: About" << endl;
		SeperatorLine();
		cout << "Written by Jake Scott" << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Map
{
public:
	const int width = 8;
	const int height = 8;
	char data[8][8] = {
		{ '#', '#', '#', '#', '#', '#', '#', '#' },
		{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
		{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
		{ '#', '#', 'P', '#', '#', '#', '#', '#' } };
	string message;
};

struct PlayerData
{
	mutex lock;
	int posX = 0;
	int posY = 0;
	bool changed = false;
};

GameState *GetNextState(State nextState)
{
	if (nextState == StMenu)
		return new Menu();
	else if (nextState == StAbout)
		return new About();
	else if (nextState == StHelp)
		return new Help();
	else if (nextState == StFameHall)
		return new FameHall();
	else if (nextState == StSelect)
		return new Select();
	else if (nextState == StGameplay_Box || nextState == StGameplay_Void || nextState == StGameplay_Water)
		return new Gameplay();
	else if (nextState == StNewScore)
		return new NewScore();

}

int main()
{
	GameState *stateObject = new Menu();
	State nextState = StMenu;
	while (nextState != StQuit)
	{
		stateObject->Step();
		nextState = stateObject->CheckTransition();
		if (nextState != StRepeat && nextState != StQuit) {
			stateObject = GetNextState(nextState);
			stateObject->Initialize(nextState);
		}
	}
}
