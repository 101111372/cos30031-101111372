#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <chrono> 
#include <ctime> 
#include <list>
#include <array>
#include <vector>
#include <map>
#include <istream>
#include <ostream>
#include <sstream>

using namespace std;

class Player;

class Comp;

class Thing;

class TContainer;

struct HighScore
{
	int score;
	string name;
	string world;
};

enum MessageTarget {
	Object,
	Enemies,
	Game
};

struct Message {
	Thing* origin;
	MessageTarget target;
	string targetName;
	string text;
};

class Blackboard {
private:
	list<Message> messages;
public:
	list<Message> Messages() {
		list<Message> outgoingMessages = messages;
		messages.clear();
		return outgoingMessages;
	}

	void PostMessage(Message message) {
		messages.push_back(message);
	}

	void PostMessage(Thing* Origin, MessageTarget Target, string TargetName, string Text) {
		Message newMessage = { Origin, Target, TargetName, Text };
		messages.push_back(newMessage);
	}
};

Blackboard* board = new Blackboard();

array<HighScore, 11> scores;


void SortHighscore()
{
	for (int n = 0; n < 10; ++n) {
		for (int i = 0; i < 10 - n; ++i) {
			if (scores[i].score < scores[i + 1].score) {
				HighScore tempScore = scores[i];
				scores[i] = scores[i + 1];
				scores[i + 1] = tempScore;
			}
		}
	}
}

void WriteScoreBinary()
{
	ofstream outFile("Highscores.dat", ios::out | ios::binary);
	if (!outFile) {
		cout << "Cannot open file!" << endl;
		return;
	}
	for (int i = 0; i < 10; i++)
		outFile.write((char *)&scores[i], sizeof(HighScore));
	outFile.close();
}

void ReadScoreBinary()
{
	ifstream inFile("Highscores.dat", ios::in | ios::binary);
	if (!inFile) {
		cout << "Cannot open file!" << endl;
		return;
	}
	for (int i = 0; i < 10; i++)
		inFile.read((char *)&scores[i], sizeof(HighScore));
	inFile.close();
}

array<string, 3> InterpretLine(string line, char delim) {
	std::size_t current, previous = 0;
	int i = 0;
	array<string, 3> result;
	current = line.find_first_of(delim);
	while (current != std::string::npos) {
		result[i] = line.substr(previous, current - previous);
		if (i >= 3)
			break;
		++i;
		previous = current + 1;
		current = line.find_first_of(delim, previous);
	}
	result[i] = line.substr(previous, current - previous);
	return result;
}

void ReadHighscoreText()
{
	string line;
	ifstream inFile;

	inFile.open("Highscores.txt");
	if (!inFile) {
		cout << "Unable to open file";
		exit(1); // terminate with error
	}

	int j = 0;
	while (inFile >> line) {
		//Print lines out for testing.
		//cout << line << endl;

		const int elements = 3;
		char delim = ',';

		array <string, elements> temp = InterpretLine(line, delim);

		if (j > 10)
			break;
		HighScore nextScore = { stoi(temp[0]), temp[1], temp[2] };
		scores[j] = nextScore;
		++j;
	}

	inFile.close();
}

void WriteHighscoreText()
{
	ofstream outFile;
	outFile.open("Highscores.txt");

	for each (HighScore HS in scores)
	{
		outFile << HS.score << ",";
		outFile << HS.name << ",";
		outFile << HS.world << endl;
	}
	outFile.close();
}

<<<<<<< Updated upstream
class Player;
=======
class Comp
{
protected:
	Thing* host;
public:
	//Containers Implement this.
	virtual void LookIn() {};

	//Containers Implement this.
	virtual void Take(Thing* Object) {};

	//Containers Implement this.
	virtual void PutIn(Thing* Object) {};

	//Containers Implement this.
	virtual Thing* FindIn(string target) {
		return nullptr;
	};

	virtual list<Thing*>* PointToContainer() {
		return nullptr;
	}

	//Containers and Portals Implement this. 
	virtual void Open(Thing* Using) {};

	//Containers and Portals Implement this.
	virtual void Close() {};

	//Enemies Implement this.
	virtual void Attack(Thing* With) {};
};
>>>>>>> Stashed changes

//Anything that can potentially be interacted with
class Thing
{
public:
	string name = "Nothing";
	string description = "Doesn't exist.";
<<<<<<< Updated upstream
	string NameOf()
=======
	//list<Comp> components;
	map<GameCommand, Comp*> actions;
	map<string, string> tags;
	//list<Thing*> contents;
	map<string, Thing*> connections;

	Thing() {}

	Thing(string Name) {
		name = Name;
	}

	bool MessageIsForMe(Message message) {
		if (message.origin == this)
			return false;
		if (message.target == Enemies && actions.count(attack) > 0)
			return true;
		if (message.target == Object && message.targetName == name)
			return true;
		return false;
	}

	void CheckMessage(Message message) {
		if (!MessageIsForMe(message))
			return;

		if (message.text == "help") {

			board->PostMessage(this, Game, "", "attack");
			cout << "The " << name << " gets ready to attack you" << endl;
		}
	}

	bool Can(GameCommand command) {
		return (actions.count(command) > 0);
	}

	void LookIn() {
		actions[look]->LookIn();
	}

	void LookAt()
>>>>>>> Stashed changes
	{
		return name;
	}
<<<<<<< Updated upstream
	void SetName(string Name)
	{
		name = Name;
=======

	bool PutIn(Thing* Object) {
		if (tags["container"] == "yes" && actions[put] > 0) {
			Comp* chosenComponent = actions[put];
			chosenComponent->PutIn(Object);
			return true;
		}
		return false;
	}

	//Returns nullptr if not a container.
	bool IsContainer()
	{
		return (tags["container"] == "yes" && actions.count(getContainer) > 0);
>>>>>>> Stashed changes
	}

	//Called if this object reacts to the players actions. Out of Scope.
	void Respond(Player* player)
	{

	}

	void LookAt()
	{
<<<<<<< Updated upstream
		cout << "Uninitialized Object" << endl;
=======
		if (host->tags["state"] == "locked" || host->tags["state"] == "alive") {
			cout << "You can't look in that, it's " << host->tags["state"] << endl;
			return;
		}

		for each (Thing* object in contents)
			cout << "  - " << object->name << endl;
>>>>>>> Stashed changes
	}
	void LookIn()
	{
		cout << "You can't look in " << name << endl;
	}
	void Open(Thing* Using)
	{
<<<<<<< Updated upstream
		cout << "Can't open " << name << endl;
=======
		cout << "You go to open the " << host->name << " using the " << Using->name << endl;
		if (host->tags["state"] == "locked" && Using->tags["opens"] == host->name) {
			host->tags["state"] = "open";
			cout << "It opens!" << endl;
		}
		else
			cout << "It doesn't seem to fit" << endl;
>>>>>>> Stashed changes
	}
	void Close()
	{
		cout << "Can't close " << name << endl;
	}
	void Attack(Thing* With)
	{
		cout << "Attacking " << name << " has no effect" << endl;
	}
	void Take(Player* player, string Target)
	{
<<<<<<< Updated upstream
		cout << name <<  " doesn't have " << Target << endl;
	}
	void PutIn(Thing* Object)
	{
		cout << name << " can't hold objects" << endl;
=======
		list<Thing*>::iterator it;
		for (it = contents.begin(); it != contents.end(); ++it) {
			if ((*it)->name == target)
				return *it;
		}
		return nullptr;
	}
};

class TKey : public Comp
{
public:
	TKey(Thing* Host, string opens) {
		host = Host;
		host->tags["opens"] = opens;
		host->tags["item"] = "yes";
>>>>>>> Stashed changes
	}
};

class Location
{
public:
	string name = "Nowhere";
	string description = "Nothing here.";
	list<Thing> contents;
	map<string, Location*> connections;
};

class Item : public Thing
{
public:
<<<<<<< Updated upstream
	Item(string Name)
	{
		name = Name;
=======
	TEnemy(Thing* Host, string strHealth, string damage) {
		host = Host;
		host->actions[attack] = this;
		host->tags["damage"] = damage;
		health = stoi(strHealth);
	}

	void Attack(Thing* With) override
	{
		int damage = 1;
		string weapon = "your hands";
		if (With->tags["damage"] != "") {
			damage += stoi(With->tags["damage"]);
			weapon = "the " + With->name;
		}

		cout << "You attack the " << host->name << " with " << weapon << " (" << damage << " damage)" << endl;
		health -= damage;

		if (health <= 0 || With->tags["kills"] == host->name) {
			host->tags["state"] = "dead";
			host->actions.erase(attack);
			cout << "The " << host->name << " is dead!" << endl;
		}
		else {
			board->PostMessage(host, Enemies, "", "help");
			board->PostMessage(host, Game, "", "attack");
			cout << "The " << host->name << " recoils and calls out for help." << endl;
		}
>>>>>>> Stashed changes
	}
};

class Player: public Thing
{
private:
	int health = 5;
public:
<<<<<<< Updated upstream
	list<Item*> inventory;
	Location* currentLocation;
=======
	list<Thing*> inventory;
	Thing* currentLocation;

>>>>>>> Stashed changes
	void PrintInventory()
	{
		cout << "Your inventory contains:" << endl;
		for each (Item* item in inventory)
		{
			cout << "- " << item->name << endl;
		}
	}
<<<<<<< Updated upstream
	void TestAddItem()
=======

	void TakeItem(Thing* object)
>>>>>>> Stashed changes
	{
		std::string itemName = "Sword";
		Item *newItem = new Item(itemName);
		inventory.push_back(newItem);
	}
<<<<<<< Updated upstream
	void TestRemoveItem()
=======

	void RemoveItem(Thing* object)
>>>>>>> Stashed changes
	{
		Item* itemToRemove = inventory.front();
		inventory.remove(itemToRemove);
	}

	//Return true of the player dies.
	void Attack(Thing* enemy) {
		int damage = 0;
		if (enemy->tags["damage"] != "")
			damage = stoi(enemy->tags["damage"]);
		health -= damage;
		cout << "The " << enemy->name << " attacks you (" << damage << " damage)! You now have " << health << " health" << endl;
	}

	bool IsDead() {
		return (health <= 0);
	}
};

enum State
{
	StRepeat,
	StMenu,
	StSelect,
	StFameHall,
	StHelp,
	StAbout,
	StGameplay_Test,
	StGameplay_Water,
	StGameplay_Box,
	StNewScore,
	StQuit,
};

class GameState
{
protected:
	State thisState = StMenu;
	State nextState = StMenu;
	void SeperatorLine()
	{
		cout << "------------------------------" << endl << endl;
	}

public:
	virtual void Step() = 0;
	virtual State CheckTransition()
	{
		if (nextState != thisState)
			return nextState;
		else
			return StRepeat;
	}
	virtual void Initialize(State state)
	{
		thisState = state;
		nextState = state;
	}
	vector<string> GetCommand()
	{
		string command = "";
		getline(cin, command);
		//Split the input into words.
		vector<string> commandWords;

		std::size_t wordPos, lastWordPos = 0;
		int i = 0;
		wordPos = command.find_first_of(' ');
		while (wordPos != std::string::npos) {
			string word = command.substr(lastWordPos, wordPos - lastWordPos);
			transform(word.begin(), word.end(), word.begin(), ::tolower);
			commandWords.push_back(word);
			++i;
			lastWordPos = wordPos + 1;
			wordPos = command.find_first_of(' ', lastWordPos);
		}
		commandWords.push_back(command.substr(lastWordPos, wordPos - lastWordPos));

		return commandWords;
	}
};

class Menu : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Main Menu" << endl;
		SeperatorLine();
		cout << "Welcome to Zorkish Adventures" << endl << endl;
		cout << "1. Select Adventure and Play" << endl;
		cout << "2. Hall of Fame" << endl;
		cout << "3. Help" << endl;
		cout << "4. About" << endl;
		cout << "5. Quit" << endl << endl;

		cout << "Select 1-5:> ";

		int choice = -1;
		cin >> choice;
		if (choice == 1)
			nextState = StSelect;
		else if (choice == 2)
			nextState = StFameHall;
		else if (choice == 3)
			nextState = StHelp;
		else if (choice == 4)
			nextState = StAbout;
		else if (choice == 5)
			nextState = StQuit;

		cout << endl << endl;
		cin.get();
	}
};

class About : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: About" << endl;
		SeperatorLine();
		cout << "Written by Jake Scott" << endl << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Help : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Help" << endl;
		SeperatorLine();
		cout << "The following commands are supported:" << endl;
		cout << "quit," << endl;
		cout << "hiscore (for testing)" << endl;
		cout << "inventory," << endl;
		cout << "[go] _, (or just n, ne, e, etc)" << endl;
		cout << "look at _," << endl;
		cout << "look in _," << endl;
		cout << "open _ [with _]," << endl;
		cout << "close _," << endl;
		cout << "attack _ with _," << endl;
		cout << "take _ [from _]," << endl;
		cout << "put _ in _," << endl;
		cout << "drop _," << endl;
		cout << "quit" << endl;
		cout << "[up arrow] to repeat last command" << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class NewScore : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: New High Score" << endl;
		SeperatorLine();
		cout << "Congratulations!" << endl << endl;

		cout << "You have made it to the Zorkish Hall of Fame" << endl << endl;

		string adventure = "Test";
		cout << "Adventure: " << adventure << endl;
		int score = 1000;
		cout << "Score: " << score << endl;
		int moves = 69;
		cout << "Moves: " << moves << endl;

		cout << "Please type your name and press Enter:" << endl;

		cout << ":> ";
		string scoreName = "Noob";
		cin >> scoreName;

		ReadHighscoreText();
		HighScore newScore = { score, scoreName, adventure };
		scores[10] = newScore;
		SortHighscore();
		WriteHighscoreText();

		//>> Add score to highscore data << ??

		nextState = StFameHall;

		cout << endl << endl;
		cin.get();
	}
};

class FameHall : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Hall of Fame" << endl;
		SeperatorLine();
		cout << "Top 10 Zorkish Adventure Champions" << endl << endl;
		string name = "NoobMaster69";
		string world = "Planet World";
		int score = 5000;
		for (int i = 0; i < 10; ++i)
			cout << (i + 1) << ". " << scores[i].name << ", " << scores[i].world << ", " << scores[i].score << endl;
		cout << endl;
		cout << "Press Enter to return to the Main Menu" << endl;

		nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Select : public GameState
{
public:
	void Step() override
	{
		cout << "Zorkish :: Select Adventure" << endl;
		SeperatorLine();
		cout << "Choose your adventure:" << endl << endl;

		cout << "1. Test World" << endl;
		cout << "2. Water World" << endl;
		cout << "3. Box World" << endl;
		cout << "0. Cancel" << endl << endl;

		cout << "Select 0-3:> ";

		int choice = -1;
		cin >> choice;
		if (choice == 1)
			nextState = StGameplay_Test;
		else if (choice == 2)
			nextState = StGameplay_Water;
		else if (choice == 3)
			nextState = StGameplay_Box;
		else
			nextState = StMenu;

		cout << endl << endl;
		cin.get();
	}
};

class Gameplay : public GameState
{
private:
	bool starting = true;
	bool moved = true;

	enum GameCommand {
		quit,
		hiscore,
		inventory,
		default,
		go,
		look,
		help,
		debugTree,
		alias
	};
public:
	string name = "Test World";
<<<<<<< Updated upstream
	list<Location> locations;
	Player* player = new Player();
=======
	list<Thing> locations;
	list<Thing> allObjects;
	Player* player;// = new Player();
>>>>>>> Stashed changes
	string previousCommand = "up up down down left right B A";
	map<string, GameCommand> mainCommands;

	void Initialize(State state) override
	{
		thisState = state;
		nextState = state;

<<<<<<< Updated upstream
		mainCommands.insert(pair<string, GameCommand>("quit", quit));
		mainCommands.insert(pair<string, GameCommand>("hiscore", hiscore));
		mainCommands.insert(pair<string, GameCommand>("inventory", inventory));
		mainCommands.insert(pair<string, GameCommand>("go", go));
		mainCommands.insert(pair<string, GameCommand>("look", look));
		mainCommands.insert(pair<string, GameCommand>("help", help));
		mainCommands.insert(pair<string, GameCommand>("debugTree", debugTree));
		mainCommands.insert(pair<string, GameCommand>("alias", alias));

=======
		mainCommands["quit"] = quit;
		mainCommands["hiscore"] = hiscore;
		mainCommands["inventory"] = inventory;
		mainCommands["go"] = go;
		mainCommands["look"] = look;
		mainCommands["open"] = open;
		mainCommands["close"] = close;
		mainCommands["attack"] = attack;
		mainCommands["take"] = take;
		mainCommands["put"] = put;
		mainCommands["drop"] = drop;
		mainCommands["help"] = help;
		mainCommands["debugTree"] = debugTree;
		mainCommands["alias"] = alias;

		player = new Player();
>>>>>>> Stashed changes
		player->currentLocation = &locations.front();
		context = player->currentLocation;

		cout << "Zorkish :: " << name << endl;
		SeperatorLine();
		cout << "This world is empty, pointless." << endl;
		starting = false;
	}

	void Help()
	{
		cout << endl << "The following commands are supported:" << endl;
		cout << "quit," << endl;
		cout << "hiscore (for testing)" << endl;
		cout << "inventory," << endl;
		cout << "go _," << endl;
		cout << "look at _," << endl;
		cout << "repeat" << endl;
		cout << "alias [action] [new word]" << endl;
		cout << "debugtree" << endl;
		cout << endl << "And these might come later:" << endl;
		cout << "look in _," << endl;
		cout << "open _ [with _]," << endl;
		cout << "close _," << endl;
		cout << "attack _ with _," << endl;
		cout << "take _ [from _]," << endl;
		cout << "put _ in _," << endl;
		cout << "drop _," << endl;
		cout << endl;
	}

<<<<<<< Updated upstream
=======
	Thing* FindTarget(string target)
	{
		//Search the location for the object.
		Thing* found = player->currentLocation->FindInside(target);
		if (found != nullptr)
			return found;
		if (context->name == target)
			return context;
		//Search the current context for the object.
		return context->FindInside(target);

	}

	void UseComponent(Thing* object, GameCommand command, vector<string> arguments)
	{
		if (object->Can(command)) {
			Comp* chosenComponent = object->actions[command];

			context = object;

			if (command == look) {
				cout << "Inside the " << object->name << " you see:" << endl;
				object->LookIn();
				//chosenComponent->LookIn();
			}
			else if (command == open) {
				if (arguments.size() > 3 && arguments[2] == "using") {//Open-object-Using-object
					for each(Thing* item in player->inventory) {
						if (item->name == arguments[3]) {
							chosenComponent->Open(item);
							return;
						}
					}
				}
				chosenComponent->Open(player);
			}
			else if (command == attack) {
				if (arguments.size() > 3 && arguments[2] == "with") {//Open-object-Using-object
					for each(Thing* item in player->inventory) {
						if (item->name == arguments[3]) {
							chosenComponent->Attack(item);
							return;
						}
					}
				}
				cout << "Attacking with latent energy only:" << endl;
				chosenComponent->Attack(player);
			}
			else if (command == close) {
				chosenComponent->Close();
				context = player;
			}
			else if (command == take) {
				Thing* target = chosenComponent->FindIn(arguments[1]);
				if (target != nullptr && target->tags["item"] == "yes") {
					chosenComponent->Take(target);
					player->inventory.push_back(target);
					cout << "You take the " << arguments[1] << " from the " << object->name << " and put it in your bag" << endl;
				}
				else
					cout << "You can't find " << arguments[1] << " in the " << context->name << endl;
			}
			else if (command == put) {
				for each(Thing* item in player->inventory) {
					if (item->name == arguments[1]) {
						player->inventory.remove(item);
						chosenComponent->PutIn(item);
						cout << "You take the " << arguments[1] << " from your bag and put it in the " << object->name << endl;
						return;
					}
				}
				cout << "You don't have that item." << endl;
			}
			else if (command == open) {
				if (arguments.size() > 3 && arguments[2] == "with") {
					for each(Thing* item in player->inventory) {
						if (item->name == arguments[3]) {
							chosenComponent->Open(item);
							return;
						}
					}
				}
				chosenComponent->Open(player);
			}
		}
		else
			cout << "You can't do that with this object." << endl;
	}

	void CheckMessage(Message message) {
		if (message.target == Game) {
			if (message.text == "attack") {
				Thing* localObject = FindTarget(message.origin->name);
				if (localObject != nullptr)
					player->Attack(message.origin);
			}
		}
	}

	void ReadMessages() {
		for each (Message message in board->Messages())
		{
			list<Thing>::iterator it;
			for (it = allObjects.begin(); it != allObjects.end(); ++it)
				it->CheckMessage(message);
			CheckMessage(message);
		}
	}

>>>>>>> Stashed changes
	void Step() override
	{
		if (moved)
			cout << "You arrive at " << player->currentLocation->description << endl;
		moved = false;

		ReadMessages();
		if (player->IsDead()) {
			nextState = StNewScore;
			cout << "You died!" << endl;
			return;
		}


		cout << "[At " << context->name << "] What will you do?:> ";

		vector<string> command = GetCommand();
		//cin >> command;

		GameCommand aliasedCommand = default;
		if (mainCommands.count(command[0]) > 0)
			aliasedCommand = mainCommands[command[0]];

		if (aliasedCommand == default) {
<<<<<<< Updated upstream
			if (command.size() > 0 && player->currentLocation->connections.count(command[0]) > 0) {
				Location *newLocation = player->currentLocation->connections[command[0]];
=======
			if (arguments.size() > 0 && player->currentLocation->connections.count(arguments[0]) > 0) {
				Thing *newLocation = player->currentLocation->connections[arguments[0]];
>>>>>>> Stashed changes
				//Check if newLocation is null here.
				player->currentLocation = newLocation;
				context = newLocation;
				moved = true;
			}
			else
<<<<<<< Updated upstream
				cout << "You don't know how to do that." << command.size() << endl;
=======
				cout << "You don't know how to do that." << endl;
>>>>>>> Stashed changes
		}
		else if (aliasedCommand == quit)
			nextState = StQuit;
		else if (aliasedCommand == hiscore)
			//Set player score data here.
			nextState = StNewScore;
		else if (aliasedCommand == inventory)
			player->PrintInventory();
		else if (aliasedCommand == help)
			Help();
		else if (aliasedCommand == look) {
			if (command.size() > 2 && command[1] == "at") {
				bool exists = false;
				for each (Thing object in player->currentLocation->contents) {
					if (object.name == command[2]) {
						cout << object.description << endl;
						exists = true;
						break;
					}
				}
				if (!exists)
					cout << "You can't see any of those around here." << endl;
			}
			else {
				cout << "Looking around you see:" << endl;
<<<<<<< Updated upstream
				for each (Thing object in player->currentLocation->contents)
					cout << "  - " << object.name << endl;
=======
				UseComponent(player->currentLocation, look, arguments);
				//player->currentLocation->actions[look]->LookIn();
			}
		}
		else if (aliasedCommand == attack && arguments.size() > 1) {
			Thing* target = FindTarget(arguments[1]);
			if (target != nullptr) {
				context = target;
				UseComponent(target, attack, arguments);
			}
			else
				cout << "You can't see any of those around here." << endl;
		}
		else if (aliasedCommand == put && arguments.size() > 3 && arguments[2] == "in") {
			Thing* target = FindTarget(arguments[3]);
			if (target != nullptr) {
				context = target;
				UseComponent(target, put, arguments);
			}
			else
				cout << "You can't see any of those around here." << endl;
		}
		else if (aliasedCommand == take) {
			Thing* from = context;
			if (arguments.size() > 3 && arguments[2] == "from")
				from = FindTarget(arguments[3]);
			context = from;

			UseComponent(context, take, arguments);
		}
		else if (aliasedCommand == drop) {
			for each(Thing* item in player->inventory) {
				if (item->name == arguments[1]) {
					player->inventory.remove(item);
					player->currentLocation->actions[put]->PutIn(item);
					return;
				}
>>>>>>> Stashed changes
			}
		}
<<<<<<< Updated upstream
		else if (aliasedCommand == go && command.size() > 1 && player->currentLocation->connections.count(command[1]) > 0) {
			Location *newLocation = player->currentLocation->connections[command[1]];
=======
		else if (aliasedCommand == open) {
			Thing* target = FindTarget(arguments[1]);
			if (target != nullptr) {
				context = target;
				UseComponent(target, open, arguments);
			}
			else
				cout << "You can't see any of those around here." << endl;
		}
		else if (aliasedCommand == go && arguments.size() > 1 && player->currentLocation->connections.count(arguments[1]) > 0) {
			Thing *newLocation = player->currentLocation->connections[arguments[1]];
>>>>>>> Stashed changes
			//Check if newLocation is null here.
			player->currentLocation = newLocation;
			context = newLocation;
			moved = true;
		}
		else if (aliasedCommand == alias && command.size() > 2) {
			if (mainCommands.count(command[1]) > 0) {
				GameCommand original = mainCommands[command[1]];
				mainCommands.insert(pair<string, GameCommand>(command[2], original));
			}
			else
				cout << "You can't remap that command, does it exist?" << endl;
		}
		else
			cout << "unknown command" << endl;

		
		if (player->IsDead()) {
			nextState = StNewScore;
			cout << "You died!" << endl;
			return;
		}

		cout << endl << endl;
		//cin.get();
	}
};

<<<<<<< Updated upstream
=======
vector<string> SplitLine(string* original, string delim) {
	std::size_t current, previous = 0;
	int i = 0;
	vector<string> result;
	current = (*original).find_first_of(delim);
	while (current != std::string::npos) {
		result.push_back((*original).substr(previous, current - previous));
		previous = current + 1;
		current = (*original).find_first_of(delim, previous);
	}
	result.push_back((*original).substr(previous, current - previous));
	return result;
}

//Split the type data (e.g. "Type=Container,State=Closed") into a map of 'variable - value'.
map<string, string> SplitTypeData(string* original) {
	transform((*original).begin(), (*original).end(), (*original).begin(), ::tolower);

	vector<string> pairs = SplitLine(original, ",");
	map<string, string> result;
	for each (string pair in pairs)
	{
		vector<string> keyValue = SplitLine(&pair, "=");
		result[keyValue[0]] = keyValue[1];
	}
	return result;
}

//Create objects inside this container recursively. Returns the index of the last 'End' line.
int AddObjectToContainer(vector<string>* lines, int i, Thing* parent, Gameplay *World)
{
	if (!parent->IsContainer()) {
		cout << "Error: Trying to add object to non-container." << endl;
		return i;
	}

	//Create the object and save it in the 'World'.
	World->allObjects.push_back(Thing());
	Thing* newObject = &(World->allObjects.back());

	//Give it a name + description.
	++i; //From "Inside:" to Name.

	transform((*lines)[i].begin(), (*lines)[i].end(), (*lines)[i].begin(), ::tolower);
	newObject->name = (*lines)[i];
	newObject->description = (*lines)[++i]; //From Name to Description.

	//Add any components to the object.
	++i; //From Description to "Type=..." OR "End" OR "Inside:" ("Inside:" should only be after 'container' type).
	while ((*lines)[i] != "Inside:" && (*lines)[i] != "End") {
		map<string, string> typeInfo = SplitTypeData(&(*lines)[i]);
		if (typeInfo.count("type") > 0) {
			if (typeInfo["type"] == "container")
				new TContainer(newObject, typeInfo["state"]);//newObject->components.push_back(TContainer(newObject, typeInfo["state"]));
			else if (typeInfo["type"] == "weapon")
				new TWeapon(newObject, typeInfo["damage"], typeInfo["kills"]);
			else if (typeInfo["type"] == "key")
				new TKey(newObject, typeInfo["opens"]);
			else if (typeInfo["type"] == "enemy")
				new TEnemy(newObject, typeInfo["health"], typeInfo["damage"]);
		}
		else
			cout << "Error while generating world. (type data format)" << endl;
		++i; //From current Type to next Type OR "End" OR "Inside:".
	}

	//Add the object to the container.
	parent->PutIn(newObject);

	//Create any objects inside this object.
	while ((*lines)[i] == "Inside:")
		i = AddObjectToContainer(lines, i, newObject, World) + 1; //From "End" of inside object, to next "Inside:" OR "End" of this object.

	return i;
}

>>>>>>> Stashed changes
void GenerateWorld(string Directory, Gameplay *World)
{
	ifstream inFile;

	inFile.open(Directory);
	if (!inFile) {
		cout << "Unable to open file";
		exit(1); // terminate with error
	}

	string nextLine;
	vector<string> lines;
	int j = 0;
	//Add each line in the file to a list of strings.
	while (inFile >> nextLine) {
		lines.push_back(nextLine);
	}

	string state = "name";
	int i = 0;
	for (i = 0; i < lines.size(); ++i)
	{
		if (lines[i] == "***Connections***") {
			break;
		}

		//Add the location to the world.
		World->locations.push_back(Thing());// newLocation);
		Thing* newLocation = &(World->locations.back());
		new TContainer(newLocation, "open");//newLocation->components.push_back(TContainer(newLocation, "open"));
		newLocation->tags["location"] = "yes";

		transform(lines[i].begin(), lines[i].end(), lines[i].begin(), ::tolower);
<<<<<<< Updated upstream
		newLocation.name = lines[i];
		newLocation.description = "";
		++i;
		while (i < lines.size() && lines[i] != "Contains:" && lines[i] != "~~~~~" && lines[i] != "*****") {
			newLocation.description.append(lines[i]);
			newLocation.description.append(" ");
			++i;
		}
		bool hasContents = true;
		while (hasContents) {
			if (lines[i] == "Contains:") {
				Thing newObject;
				++i;
				transform(lines[i].begin(), lines[i].end(), lines[i].begin(), ::tolower);
				newObject.name = lines[i];
				newObject.description = "";
				++i;
				while (i < lines.size() && lines[i] != "Contains:" && lines[i] != "~~~~~" && lines[i] != "*****") {
					newObject.description.append(lines[i]);
					newObject.description.append(" ");
					++i;
				}
				newLocation.contents.push_back(newObject);
			}
			else
				hasContents = false;
		}
		World->locations.push_back(newLocation);

		if (lines[i] == "*****") {
			++i;
			break;
		}
=======
		newLocation->name = lines[i];
		newLocation->description = lines[++i]; //From Name to Description.

		//Get the container for this location, and begin adding objects to it (recursively).
		while (lines[++i] == "Contains:") //From Description OR "End" to "Contains:" OR "~EndLocation~" OR "***Connections***"
			i = AddObjectToContainer(&lines, i, newLocation, World);
>>>>>>> Stashed changes
	}
	//Generate connections between locations.
	while (i < lines.size())
	{
		transform(lines[i].begin(), lines[i].end(), lines[i].begin(), ::tolower);
		array<string, 3> data = InterpretLine(lines[i], '>');

		list<Thing>::iterator it1;
		list<Thing>::iterator it2;

		for (it1 = World->locations.begin(); it1 != World->locations.end(); ++it1)
		{
			if (it1->name == data[0]) {
				for (it2 = World->locations.begin(); it2 != World->locations.end(); ++it2)
				{
					if (it2->name == data[2]) {
						it1->connections[data[1]] = &(*it2);
					}
				}
			}
		}
		++i;
	}

	inFile.close();
}

GameState *GetNextState(State nextState)
{
	if (nextState == StMenu)
		return new Menu();
	else if (nextState == StAbout)
		return new About();
	else if (nextState == StHelp)
		return new Help();
	else if (nextState == StFameHall)
		return new FameHall();
	else if (nextState == StSelect)
		return new Select();
	else if (nextState == StGameplay_Test || nextState == StGameplay_Box || nextState == StGameplay_Water) {
		Gameplay *world = new Gameplay();
		GenerateWorld("PlainWorld.txt", world);
		//Change directory based on choice here /\.
		return world;
	}
	else if (nextState == StNewScore)
		return new NewScore();

}

int main()
{
	/*
	for (int i = 0; i < 10; ++i) {
		scores[i] = { (i + 1) * 7 + i, "Test", "World" };
	}
	WriteScoreBinary();
	*/
	ReadHighscoreText();
	SortHighscore();
	WriteHighscoreText();
	//ReadScoreBinary();

	GameState *stateObject = new Menu();
	State nextState = StMenu;
	while (nextState != StQuit)
	{
		stateObject->Step();
		nextState = stateObject->CheckTransition();
		if (nextState != StRepeat && nextState != StQuit) {
			stateObject = GetNextState(nextState);
			stateObject->Initialize(nextState);
		}
	}
}
