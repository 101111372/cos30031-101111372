#include <iostream>
#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <string>
#include <map>
#include <vector>

using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

struct Sprite {
	SDL_Rect rect;
	SDL_Texture* image = nullptr;
};

class pVector
{
public:
	float x = 0;
	float y = 0;

	pVector() {

	}

	pVector(float X, float Y) {
		x = X;
		y = Y;
	}

	pVector operator + (pVector const &other) {
		return pVector(x + other.x, y + other.y);
	}

	pVector operator - (pVector const &other) {
		return pVector(x - other.x, y - other.y);
	}

	pVector operator * (float value) {
		return pVector(x * value, y * value);
	}

	pVector operator * (pVector const &other) {
		return pVector(x * other.x, y * other.y);
	}

	float Magnitude() {
		return sqrtf(powf(x, 2) + powf(y, 2));
	}

	float SquaredMagnitude() {
		return powf(x, 2) + powf(y, 2);
	}

	//Changes the vector to have a magnitude of 1.
	void Normalize() {
		//Divide x and y by magnitude
		float mag = Magnitude();
		if (mag == 0)
			return;
		x = x / mag;
		y = y / mag;
	}

	//Returns a new vector with the same direction but a magnitude of 1.
	pVector Normalized() {
		float mag = Magnitude();
		if (mag == 0)
			return pVector();
		return pVector(x / mag, y / mag);
	}

	bool InsideBox(pVector origin, pVector size) {
		return !((x < origin.x || x > origin.x + size.x) || (y < origin.y || y > origin.y + size.y));
	}

	bool InsideBox(SDL_Rect box) {
		return InsideBox(pVector(box.x, box.y), pVector(box.w, box.h));
	}
};

float Distance(pVector p1, pVector p2) {

	/*
	float deltaX = (p1.x - p2.x);
	float deltaY = (p1.y - p2.y);
	float PowX = powf(deltaX, 2);
	float PowY = powf(deltaY, 2);
	float sum = PowX + PowY;
	float dist = sqrt(sum);
	dist = sqrtf(powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2));

	return dist;
	*/

	return sqrtf(powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2));
}

float SquaredDistance(pVector p1, pVector p2) {
	return powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2);
}

float Clamp(float val, float min, float max) {
	if (val < min)
		return min;
	if (val > max)
		return max;
	return val;
}

float Dot(pVector v1, pVector v2) {
	return (v1.x * v2.x) + (v1.y * v2.y);
}

pVector NearestPointOnFiniteLine(pVector start, pVector end, pVector pnt)
{
	pVector line = (end - start);
	float len = line.Magnitude();
	line.Normalize();

	pVector v = pnt - start;
	float d = Dot(v, line);
	d = Clamp(d, 0, len);
	return start + line * d;
}


void Input(SDL_Event& e);
void Update();
void Render();
bool Init();                                                // Starts up SDL and creates window
bool LoadMedia();                                           // Loads media
void CloseSDL();                                            // Frees media and shuts down SDL
void RenderImage(Sprite sprite, pVector ScreenPos);
void RenderScaledImage(Sprite sprite, pVector ScreenPos, pVector Size);
class Body;

SDL_Window* gWindow = nullptr;                              // Window to render to
SDL_Renderer* gRenderer = nullptr;							// Renderer
map<int, bool> gToggles;
int controlledObject = 0;
Sprite gBackground;
Sprite gLeft;
Sprite gIdle;
Sprite gRight;
Sprite gTest;
vector<Body*> gAllBodies;
bool gAlignMode = false;
bool quit = false;											// Main loop flag

int main(int argc, char* args[])
{
	cout << "Window: " << gWindow << " Renderer: " << gRenderer << endl;
	SDL_Event e;        // Event handler
	if (Init() && LoadMedia()) {

		// Main game loop
		while (!quit) {
			cout << "Window: " << gWindow << " Renderer: " << gRenderer << endl;
			Input(e);
			Update();
			Render();
		}
	}
	else
		return 1;

	CloseSDL(); // Free resources and close SDL

	return 0;
}
enum BodyType {
	Box,
	Circle,
	None
};

class Body
{
	//Everything is public for easier testing.
public:
	BodyType type = None;
	//Top-left position of the object.
	pVector pos;
	//Radius of the circle object.
	float radius;
	//Size of the box object from the top-left extending down and right.
	pVector size;
	bool useSprite = false;
	Sprite sprite;
	//Used for circles mostly.
	pVector spriteOffset;
	pVector velocity;

	Body(pVector Pos, pVector Size, Sprite Image) {
		type = Box;
		pos = Pos;
		size = Size;
		sprite = Image;
		useSprite = true;
	}

	Body(pVector Pos, pVector Size) {
		type = Box;
		pos = Pos;
		size = Size;
	}

	Body(pVector Pos, float Radius, Sprite Image) {
		type = Circle;
		pos = Pos;
		radius = Radius;
		size = pVector(radius * 2, radius * 2);
		spriteOffset = pVector(-Radius, -Radius);
		sprite = Image;
		useSprite = true;
	}

	Body(pVector Pos, float Radius) {
		type = Circle;
		pos = Pos;
		radius = Radius;
		size = pVector(radius * 2, radius * 2);
		spriteOffset = pVector(-Radius, -Radius);
	}

	void Draw() {
		if (useSprite)
			RenderScaledImage(sprite, pos + spriteOffset, size);
	}

	void AutoMove() {
		if (gAlignMode)
			MoveAndAlign(velocity);
		else
			Move(velocity);
		Wrap();
	}

	void Move(pVector Motion) {
		pVector origin = pos;
		pos = pos + Motion;

		for each (Body* other in gAllBodies)
		{
			if (other == this)
				continue;
			if (CheckCollision(other)) {
				pos = origin;
				break;
			}
		}
	}

	//Unfinished
	void MoveAndAlign(pVector Motion) {
		pVector origin = pos;
		pos = pos + Motion;

		for each (Body* other in gAllBodies)
		{
			if (other == this)
				continue;
			if (CheckCollision(other)) {
				//Instead of this, loop, moving by a magnitude of -1 until there isn't a collision.
				pos = origin;
				break;
			}
		}
	}

	void Wrap() {
		if (pos.x > SCREEN_WIDTH)
			pos.x = 0;
		else if (pos.x < 0)
			pos.x = SCREEN_WIDTH;

		if (pos.y > SCREEN_HEIGHT)
			pos.y = 0;
		else if (pos.y < 0)
			pos.y = SCREEN_HEIGHT;
	}

	bool CheckCollision(Body* other) {
		if (other->type == type) {
			if (type == Circle)
				return CircleCollidesCircle(other);
			else
				return BoxCollidesBox(other);
		}
		else {
			if (type == Box)
				return CircleCollidesBox(other);
			else
				return other->CircleCollidesBox(this);
		}
	}

	//Returns true if the distance between the two circles is less than their combined radiuses.
	bool CircleCollidesCircle(Body* other) {
		return (Distance(pos, other->pos) < radius + other->radius);
	}

	//Assumes object being called on is a box type.
	bool CircleCollidesBox(Body* circle) {
		//No collision if bounding boxes do not intersect.
		if (!BoxCollidesBox(circle->pos - pVector(circle->radius, circle->radius), circle->size)) {
			//circle->sprite.rect.x = 0;
			return false;
		}

		circle->sprite.rect.x = 32;
		//If circles center is within vertical or horizontal projection, it is colliding (because bounding box collided).
		if (circle->pos.x > pos.x && circle->pos.x < pos.x + size.x)
			return true;
		if (circle->pos.y > pos.y && circle->pos.y < pos.y + size.y)
			return true;

		circle->sprite.rect.x = 64;
		//Otherwise the only collision can be with one of the corners of the box.
		float SquaredRadius = circle->radius * circle->radius; //Squareroot is very slow.
		if (SquaredDistance(pos, circle->pos) < SquaredRadius
			|| SquaredDistance(pos + pVector(size.x, 0), circle->pos) < SquaredRadius
			|| SquaredDistance(pos + pVector(0, size.y), circle->pos) < SquaredRadius
			|| SquaredDistance(pos + pVector(size.x, size.y), circle->pos) < SquaredRadius) {
			circle->sprite.rect.x = 16;
			return true;
		}

		circle->sprite.rect.x = 96;
		return false;
	}

	//Returns true if either box intersects with the other.
	bool BoxCollidesBox(Body* other) {
		return ((pos.y < other->pos.y + other->size.y)
			&& (pos.y + size.y > other->pos.y)
			&& (pos.x < other->pos.x + other->size.x)
			&& (pos.x + size.x > other->pos.x));
	}

	//Returns true if either box intersects with the other. Takes a position and size instead of a body.
	bool BoxCollidesBox(pVector oPos, pVector oSize) {
		return ((pos.y < oPos.y + oSize.y)
			&& (pos.y + size.y > oPos.y)
			&& (pos.x < oPos.x + oSize.x)
			&& (pos.x + size.x > oPos.x));
	}
};

void Input(SDL_Event& e) {
	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT)
			quit = true;
		else if (e.type == SDL_KEYDOWN)
		{
			//Toggle images based on key press
			switch (e.key.keysym.sym)
			{
			case SDLK_1:
				controlledObject -= 1;
				if (controlledObject < 0)
					controlledObject = gAllBodies.size() - 1;
				break;
			case SDLK_2:
				controlledObject += 1;
				if (controlledObject >= gAllBodies.size())
					controlledObject = 0;
				break;
			case SDLK_w:
				gAllBodies[controlledObject]->velocity.y -= 0.01f;
				break;
			case SDLK_s:
				gAllBodies[controlledObject]->velocity.y += 0.01f;
				break;
			case SDLK_a:
				gAllBodies[controlledObject]->velocity.x -= 0.01f;
				break;
			case SDLK_d:
				gAllBodies[controlledObject]->velocity.x += 0.01f;
				break;
			case SDLK_SPACE:
				gAllBodies[controlledObject]->velocity = pVector(0, 0);
				break;
			}
		}
	}
}

void Update() {

	for each (Body* body in gAllBodies)
	{
		body->AutoMove();
	}
}

//Render the image using the position and dimensions of the given rect.
void RenderScaledImage(Sprite sprite, SDL_Rect* ScreenRect) {
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), ScreenRect);
}

void RenderScaledImage(Sprite sprite, pVector ScreenPos, pVector Size) {
	SDL_Rect pos = { ScreenPos.x, ScreenPos.y, Size.x, Size.y };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render the image using x and y screen positions, and its default dimensions. (x=0=left, y=0=top)
void RenderImageAt(Sprite sprite, int x, int y) {
	SDL_Rect pos = { x, y, sprite.rect.w, sprite.rect.h };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render the image using x and y screen positions, and custom dimensions. (x=0=left, y=0=top)
void RenderScaledImageAt(Sprite sprite, int x, int y, int w, int h) {
	SDL_Rect pos = { x, y, w, h };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render the image using the position of rect, and its default dimensions.
void RenderImage(Sprite sprite, SDL_Rect* ScreenPos) {
	SDL_Rect pos = *ScreenPos;
	pos.w = sprite.rect.w;
	pos.h = sprite.rect.h;
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

//Render the image using the pVector position, and its default dimensions.
void RenderImage(Sprite sprite, pVector ScreenPos) {
	SDL_Rect pos = { ScreenPos.x, ScreenPos.y, sprite.rect.w, sprite.rect.h };
	SDL_RenderCopy(gRenderer, sprite.image, &(sprite.rect), &pos);
}

void RenderBackground(Sprite sprite) {
	SDL_RenderCopy(gRenderer, sprite.image, NULL, NULL);
}

void Render() {

	//Clear screen
	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(gRenderer);

	if (gToggles[0])
		RenderBackground(gBackground);
	if (gToggles[1])
		RenderScaledImageAt(gLeft, 470, 300, 128, 128);
	if (gToggles[2])
		RenderScaledImageAt(gIdle, 250, 300, 128, 128);
	if (gToggles[3])
		RenderScaledImageAt(gRight, 20, 300, 128, 128);

	for each (Body* body in gAllBodies)
	{
		body->Draw();
	}

	//Update screen
	SDL_RenderPresent(gRenderer);
}

bool Init()
{
	// Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL was not initialized. SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	// Create window
	gWindow = SDL_CreateWindow("Sprite Testing", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (gWindow == nullptr) {
		printf("Window was not created. SDL_Error: %s\n", SDL_GetError());
		return false;
	}

	// Create Renderer for window
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
	if (gRenderer == nullptr) {
		printf("Renderer was not created. SDL Error: %s", SDL_GetError());
		return false;
	}

	SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

	//Initialize PNG loading
	if (IMG_Init(IMG_INIT_PNG) == 0) {
		printf("SDL_image was not initialized. SDL_image Error: %s\n", IMG_GetError());
		return false;
	}

	gToggles[0] = false;
	gToggles[1] = false;
	gToggles[2] = false;
	gToggles[3] = false;

}

//Create a texture by saving the pixels loaded onto a temporary surface.
void LoadSprite(string path, Sprite* sprite) {
	SDL_Surface* tempSurface = IMG_Load(path.c_str());

	if (tempSurface != nullptr) {
		//Create texture from surface pixels
		sprite->image = SDL_CreateTextureFromSurface(gRenderer, tempSurface);
		if (sprite->image != nullptr) {
			//Set the default sprite to 'cut' from the sheet to the size of the sprite.
			sprite->rect.x = 0;
			sprite->rect.y = 0;
			sprite->rect.w = tempSurface->w;
			sprite->rect.h = tempSurface->h;
		}
		else
			printf("Unable to create texture from %s. SDL Error: %s\n", path.c_str(), SDL_GetError());
		SDL_FreeSurface(tempSurface);
	}
	else
		printf("Unable to load image %s. SDL_image Error: %s\n", path.c_str(), IMG_GetError());
}

bool LoadMedia()
{
	//LoadSprite("../media/Background.png", &gBackground);
	LoadSprite("../media/Table.png", &gBackground);
	LoadSprite("../media/CharSheet.png", &gLeft);
	Sprite WhiteBall;
	LoadSprite("../media/BallSheet.png", &WhiteBall);
	LoadSprite("../media/TestSheet.png", &gTest);


	if (gBackground.image == nullptr || gLeft.image == nullptr || WhiteBall.image == nullptr)
		return false;

	//Cut Character Sheet
	gLeft.rect.w = 32;
	gIdle = gLeft;
	gIdle.rect.x = 32;
	gRight = gLeft;
	gRight.rect.x = 64;

	//Cut Test Ball
	gTest.rect.w = 32;

	//Cut Ball Sheet
	WhiteBall.rect.w = 32;
	Sprite RedBall = { WhiteBall.rect, WhiteBall.image };
	RedBall.rect.x = 32;
	Sprite EightBall = { WhiteBall.rect, WhiteBall.image };
	EightBall.rect.x = 64;


	//Create bodies
	gAllBodies.push_back(new Body(pVector(40, 40), 32, gTest));
	gAllBodies.push_back(new Body(pVector(150, 300), 64, RedBall));
	gAllBodies.push_back(new Body(pVector(350, 70), 32, EightBall));

	gAllBodies.push_back(new Body(pVector(200, 40), pVector(32, 32), gLeft));
	gAllBodies.push_back(new Body(pVector(350, 300), pVector(32, 32), gRight));
	gAllBodies.push_back(new Body(pVector(400, 200), pVector(64, 64), gIdle));

	gAllBodies[0]->velocity = pVector(2, 1);
	
	return true;
}

void CloseSDL()
{
	//Destroy window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = nullptr;
	gRenderer = nullptr;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}
