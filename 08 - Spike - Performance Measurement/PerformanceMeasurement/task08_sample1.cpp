#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <string>
#include <array>

using namespace std;
using namespace std::chrono;


// - count char using slow repeated string::find_first_of
double count_char_using_find_first_of(string s, char delim)
{
    int count = 0;
    // note: string::size_type pos = s.find_first_of(delim);
    auto pos = s.find_first_of(delim);
    while ( (pos = s.find_first_of(delim, pos)) != string::npos)
    {
        count++;
        pos++;
    }
	return count; //count;
}

// - count char using fast std::count
int count_char_using_count(string s, char delim)
{
	return count(s.begin(), s.end(), delim);
}

void exponential_count_comparison(string test_string, char delim, unsigned long long max_ramp)
{
	cout << " << Exponential String Count Ramp-up Comparison >> " << endl;
	cout << "Searching string of size: " << test_string.length() << " for delimiter '" << delim << "'." << endl;
	cout << "Count of delimiter: '" << count(test_string.begin(), test_string.end(), delim) << endl;
	cout << "Count of delimiter: '" << count_char_using_find_first_of(test_string, delim) << endl;
	int total;
	array<long long, 10> results;
	int i = 0;
	for (auto size = 1ull; size < max_ramp; size *= 100)
	{
		auto start = steady_clock::now();
		for (int i = 0; i < size; ++i)
		{
			total = count_char_using_find_first_of(test_string, delim);
		}
		auto end = steady_clock::now();
		duration<double> diff = end - start;
		long long nano_time = duration_cast<nanoseconds>(diff).count();
		results[i++] = nano_time;
		cout << " - Find First >> iterations: " << size << ", time: " << nano_time << "ns";
		cout << ", time/count: " << nano_time / size << "ns/count" << endl;


		start = steady_clock::now();
		for (int i = 0; i < size; ++i)
		{
			count(test_string.begin(), test_string.end(), delim);
			//total = count_char_using_count(test_string, delim);
		}
		end = steady_clock::now();
		diff = end - start;
		nano_time = duration_cast<nanoseconds>(diff).count();
		results[i++] = nano_time;
		cout << " - std::Count >> iterations: " << size << ", time: " << nano_time << "ns";
		cout << ", time/count: " << nano_time / size << "ns/count" << endl;
	}

	cout << "Copyable Results:" << endl;
	cout << "--First:" << endl;
	for (i = 0; i < 6; i += 2)
		cout << results[i] << endl;
	cout << "--Count:" << endl;
	for (i = 1; i < 6; i += 2)
		cout << results[i] << endl;

	cout << "done." << endl;
}


void string_length_comparison_controller(unsigned int max_string_size, unsigned int delim_repeats)
{
	// Compare the two different methods of counting in a string
	// - show result in nanoseconds?
	string s1 = "This is a really simple string but it will do for testing.";
	string s2 = " abcdefghijklmnopqrstuvwxyz.";
	string s3 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris auctor in tellus et porta. Fusce metus enim, cursus quis tortor et, fermentum porta nisi. Praesent maximus eleifend lectus, eget blandit.";

	string* str_to_use = &s3;
	string s0 = *str_to_use;

	cout << "<<<<< String Lengths >>>>>" << endl;

	for (int i = s0.length(); i <= max_string_size; i = i*2)
		cout << i << endl;

	cout << "<<<<< First Starts Here >>>>>" << endl;

	//Loop
	while (s0.length() < max_string_size)
	{
		//Repeat the count using different delimiters.
		for (int i = 0; i < delim_repeats && i < s2.length(); ++i)
		{
			char delim = s2.at(i + (rand() % 20));
			int total = 0;
			auto start = steady_clock::now();
			for (int i = 0; i < 100; ++i)
			{
				total = count_char_using_find_first_of(s0, delim);
			}
			auto end = steady_clock::now();
			duration<double> diff = end - start;
			long long nano_time = duration_cast<nanoseconds>(diff).count();
			cout << nano_time << endl;
		}
		s0.append(s0);
	}

	cout << "<<<<< Count Starts Here >>>>>" << endl;
	s0 = *str_to_use;

	while (s0.length() < max_string_size)
	{
		//Repeat the count using different delimiters.
		for (int i = 0; i < delim_repeats && i < s2.length(); ++i)
		{
			char delim = s2.at(i + (rand() % 20));
			int total = 0;
			auto start = steady_clock::now();
			for (int i = 0; i < 100; ++i)
			{
				total = count(s0.begin(), s0.end(), delim);
			}
			//cout << "Total: " << total << endl;
			auto end = steady_clock::now();
			duration<double> diff = end - start;
			long long nano_time = duration_cast<nanoseconds>(diff).count();
			cout << nano_time << endl;
		}
		s0.append(s0);
	}
}


void exponential_rampup_test()
{
    cout << " << Exponential Ramp-up Test >> " << endl;
    int total;
	array<long long, 10> results;
	int i = 0;
    for (auto size = 1ull; size < 1000000000ull; size *= 100) 
    {
        // 1. get start time
        auto start = steady_clock::now();
        // 2. do some work (create, fill, find sum)
        vector<int> v(size, 42);
        total = accumulate(v.begin(), v.end(), 0u); 
        // 3. show duration time
        auto end = steady_clock::now();
        duration<double> diff = end - start;
		long long nano_time = duration_cast<nanoseconds>(diff).count();
		results[i++] = nano_time;
        cout << " - size: " << size << ", time: " << nano_time << " s";
        cout << ", time/int: " << nano_time / size << "ns/int" << endl;
    }

	cout << "Copyable Results:" << endl;
	for (i = 0; i < 5; ++i)
		cout << results[i] << endl;

    cout << "done." << endl;
}

void linear_rampup_test()
{
    cout << " << Linear Ramp-up Test >> " << endl;
    int total;
	array<long long, 10> results;
    for (auto size = 1; size <= 5; size += 1) 
    {
        int vec_size = size * 10000;
        // 1. get start time
        auto start = steady_clock::now();
        // 2. do some work (create, fill, find sum)
        vector<int> v(vec_size, 42);
        // std::accumulate (from <numeric>) collects from begin, to end
        // - in this case (default) it is the sum total of all the values in v
        total = accumulate(v.begin(), v.end(), 0u);
        // 3. show duration time
        auto end = steady_clock::now();
        duration<double> diff = end - start;
		long long nano_time = duration_cast<nanoseconds>(diff).count();
		results[size - 1] = nano_time;
        cout << " - size: " << vec_size << ", time: " << nano_time << "ns";
        cout << ", time/int: " << nano_time / vec_size << "ns/int" << endl;
    }

	cout << "Copyable Results:" << endl;
	for (auto i = 0; i < 5; ++i)
		cout << results[i] << endl;

    cout << "done." << endl;
}



int main()
{
    // Simple wrapper around a linaer set of time tests
    linear_rampup_test();

    // Simple wrapper around an exponential set of time tests
    exponential_rampup_test();

	//exponential_count_comparison("abcdefghijklmnopqrstuvwxyz. ", 'a', 1000000ull);
	string_length_comparison_controller(1000100, 1);
    //count_char_using_find_first_of(s1, 's');
    //cout << "result: " << result << endl;    

    //count_char_using_count(s1, 's');
    //cout << "result: " << result << endl;    
    
	system("pause");
}

