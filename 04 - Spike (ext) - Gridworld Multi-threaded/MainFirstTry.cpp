/*
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

const int MapSizeX = 8;
const int MapSizeY = 8;

class Map
{
public:
	mutex lock;
	char data[MapSizeX][MapSizeY] = {
		{ '#', '#', '#', '#', '#', '#', '#', '#' },
		{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
		{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
		{ '#', '#', 'P', '#', '#', '#', '#', '#' } };
};

struct Input
{
	mutex lock;
	int deltaPosX = 0;
	int deltaPosY = 0;
	int special = -1; //Set to '0' to quit
};

struct PlayerData
{
	mutex lock;
	int posX = 0;
	int posY = 0;
	bool changed = false;
	string message;
};

///<summary>
///Loop through the given 'map' (2D array of chars) and print each character to the screen,
///following each X point with a space, and each Y line with a new line.
///</summary> 
void DrawMap(Map* map) {//char Map[MapSizeY][MapSizeX]) {
	for (int j = 0; j < MapSizeY; j++) {
		for (int k = 0; k < MapSizeX; k++) {
			cout << map->data[j][k] << ' ';
		}
		cout << endl;
	}
}

///<summary>
///Wait for a string input, interpret a direction (like 'n' into dy = -1) or action (like 'q' into quit) from it,
///and then return the interpreted input.
///</summary> 
void GetInput(Input* userIn)
{
	//   N
	// W   E
	//   S

	cout << "Please enter a direction (N/S/E/W), or Q to quit: ";

	string textInput;
	getline(cin, textInput);

	transform(textInput.begin(), textInput.end(), textInput.begin(), ::toupper);

	userIn->lock.lock();

	if (textInput == "Q") {
		userIn->special = 0;
	}
	else if (textInput == "W")
	{
		userIn->deltaPosY = -1;
	}
	else if (textInput == "E")
	{
		userIn->deltaPosY = 1;
	}
	else if (textInput == "N")
	{
		userIn->deltaPosX = -1;
	}
	else if (textInput == "S")
	{
		userIn->deltaPosX = 1;
	}

	userIn->lock.unlock();
}

///<summary>
///Take the user input directly, and the player position and map as pointers,
///and use the input to update the game state. Returns true if the game should end.
///</summary> 
void Update(bool* done, Input* userIn, PlayerData* player, Map* map)
{
	userIn->lock.lock();
	if (userIn->special == 0)
	{
		*done = true; //Special '0' is the 'Quit' action.
	}

	if (userIn->deltaPosX == 0 || userIn->deltaPosY == 0)
	{
		return;
	}
	userIn->lock.unlock();

	//data->lock.lock();
	//int newPosX = data->player;
	int newPosX = player->posX + userIn->deltaPosX;
	int newPosY = player->posY + userIn->deltaPosY;

	player->changed = true;

	if ((newPosX >= 0 && newPosX < MapSizeX) && (newPosY >= 0 && newPosY < MapSizeY)) {
		map->lock.lock();
		if (map->data[newPosX][newPosY] != '#') {

			map->data[player->posX][player->posY] = ' ';

			if (map->data[newPosX][newPosY] == 'G') {
				player->message = "You reached the gold!!!";
				*done = true;
			}
			else if (map->data[newPosX][newPosY] == 'D') {
				player->message = "You fell into a pit - you're dead!!!";
				*done = true;
			}

			map->data[newPosX][newPosY] = 'P';
			map->lock.unlock();

			player->posX = newPosX;
			player->posY = newPosY;
		}
		else {
			player->message = "You run at the wall. It doesn't move.";
		}
	}
	else {
		player->message = "You step out of the grid world, and float endlessly through a void function...";
	}
	player->lock.unlock();
}

void DrawLoop(bool* done, Map* map, PlayerData* player)
{
	while (*done != true)
	{
		player->lock.lock();
		if (player->changed)
		{
			player->changed = false;
			map->lock.lock();
			DrawMap(map);
			map->lock.unlock();
			cout << player->message << endl;
		}
		player->lock.unlock();
	}
}

void UpdateLoop(bool* done, Input* userIn, PlayerData* player, Map* map)
{
	while (*done != true)
	{
		//Update(done, userIn, data);
	}
}

void InputLoop(bool* done, Input* userIn)
{
	while (*done != true)
	{
		GetInput(userIn);
	}
}

/*
int main()
{

	bool* done = new bool();
	*done = false;
	int* timer = new int();
	*timer = 0;
	Input* userIn;
	Map* map = new Map();
	PlayerData* player = new PlayerData();

	//Find the start position, set the players coordinates.
	for (int j = 0; j < MapSizeX; j++) {
		for (int k = 0; k < MapSizeY; k++) {
			if (map->data[j][k] == 'P') {
				player->posX = j;
				player->posY = k;
			}
		}
	}

	cout << "WELCOME TO GRIDWORLD!" << endl << endl;
	cout << "Compass:" << endl << "   N" << endl << " W   E" << endl << "   S" << endl << endl;


	std::thread updateThread(UpdateLoop, done, userIn, player, map);
	std::thread inputThread(InputLoop, done, userIn);
	std::thread drawThread(DrawLoop, done, map, player);

	while (*done != false)
	{
		++timer;
	}

	updateThread.join();
	inputThread.join();
	drawThread.join();

	/*
	while (done == false) {
		DrawMap(map);
		GetInput(userIn);
		Update(&done, userIn, player, map);

		if (done)
			system("pause");
	}

	return 0;
}
*/