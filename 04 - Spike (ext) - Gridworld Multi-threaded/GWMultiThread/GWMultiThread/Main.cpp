// thread example
#include <iostream>
#include <thread>
#include <mutex>
#include <string>
#include <algorithm>
#include <fstream>
#include <chrono> 
#include <ctime> 

using namespace std;

const int MapSizeX = 8;
const int MapSizeY = 8;

class Map
{
public:
	char data[MapSizeX][MapSizeY] = {
		{ '#', '#', '#', '#', '#', '#', '#', '#' },
		{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
		{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
		{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
		{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
		{ '#', '#', 'P', '#', '#', '#', '#', '#' } };
	string message;
};

class Input
{
private:
	mutex lock;
	int deltaPosX = 0;
	int deltaPosY = 0;
	int special = -1; //Set to '0' to quit
	bool done = false;

public:
	void EndExecution()
	{
		lock.lock();
		done = true;
		lock.unlock();
	}

	void MovePlayer(int newX, int newY)
	{
		lock.lock();
		deltaPosX = newX;
		deltaPosY = newY;
		lock.unlock();
	}

	void SetSpecial(int actionIndex)
	{
		lock.lock();
		special = actionIndex;
		lock.unlock();
	}

	int deltaX()
	{
		lock.lock();
		int result = deltaPosX;
		lock.unlock();
		return result;
	}

	int deltaY()
	{
		lock.lock();
		int result = deltaPosY;
		lock.unlock();
		return result;
	}

	int GetSpecial()
	{
		lock.lock();
		int result = special;
		lock.unlock();
		return result;
	}

	bool IsDone()
	{
		lock.lock();
		if (done)
		{
			lock.unlock();
			return true;
		}
		lock.unlock();
		return false;
	}
};

struct PlayerData
{
	mutex lock;
	int posX = 0;
	int posY = 0;
	bool changed = false;
};

///<summary>
///Loop through the given 'map' (2D array of chars) and print each character to the screen,
///following each X point with a space, and each Y line with a new line.
///</summary> 
void DrawMap(Map* map, int timer) {//char Map[MapSizeY][MapSizeX]) {
	system("CLS");

	cout << "WELCOME TO GRIDWORLD! Gametime: " << timer << endl << endl;
	cout << "Compass:" << endl << "   N" << endl << " W   E" << endl << "   S" << endl << endl;

	for (int y = 0; y < MapSizeY; y++) {
		for (int x = 0; x < MapSizeX; x++) {
			cout << map->data[y][x] << ' ';
		}
		cout << endl;
	}

	cout << map->message << endl;

	cout << "Please enter a direction (N/S/E/W), or Q to quit: ";
}

///<summary>
///Wait for a string input, interpret a direction (like 'n' into dy = -1) or action (like 'q' into quit) from it,
///and then return the interpreted input.
///</summary> 
void GetInput(Input* userIn)
{
	string textInput;
	textInput = fgetchar();

	transform(textInput.begin(), textInput.end(), textInput.begin(), ::toupper);

	//   N
	// W   E
	//   S

	if (textInput == "Q") {
		userIn->SetSpecial(0);
	}
	else if (textInput == "W")
	{
		userIn->MovePlayer(-1, 0);
	}
	else if (textInput == "E")
	{
		userIn->MovePlayer(1, 0);
	}
	else if (textInput == "N")
	{
		userIn->MovePlayer(0, -1);
	}
	else if (textInput == "S")
	{
		userIn->MovePlayer(0, 1);
	}
}

///<summary>
///Take the user input directly, and the player position and map as pointers,
///and use the input to update the game state. Returns true if the game should end.
///</summary> 
void Update(Input* userIn, PlayerData* player, Map* map)
{
	if (userIn->GetSpecial() == 0)
	{

		map->message = "~ ~ ~ Game Ended. Press Enter to Finish. ~ ~ ~";
		userIn->EndExecution(); //Special '0' is the 'Quit' action.
		return;
	}

	int deltaX = userIn->deltaX();
	int deltaY = userIn->deltaY();

	if (deltaX == 0 && deltaY == 0)
	{
		return;
	}

	int newPosX = player->posX + deltaX;
	int newPosY = player->posY + deltaY;

	player->changed = true;

	if ((newPosX >= 0 && newPosX < MapSizeX) && (newPosY >= 0 && newPosY < MapSizeY)) {
		if (map->data[newPosY][newPosX] != '#') {

			map->data[player->posY][player->posX] = ' ';

			if (map->data[newPosY][newPosX] == 'G') {
				map->message = "You reached the gold!!! (Press Enter)";
				userIn->EndExecution();
			}
			else if (map->data[newPosY][newPosX] == 'D') {
				map->message = "You fell into a pit - you're dead!!! (Press Enter)";
				userIn->EndExecution();
			}
			else {
				map->message = "";
			}

			map->data[newPosY][newPosX] = 'P';

			player->posX = newPosX;
			player->posY = newPosY;
		}
		else {
			map->message = "You run at the wall. It doesn't move.";
		}
	}
	else {
		map->message = "You step out of the grid world, and float endlessly through a void function...";
		userIn->EndExecution();
	}
}

void InputLoop(Input* userIn)
{
	while (userIn->IsDone() == false)
	{
		GetInput(userIn);
	}
}

int main()
{
	int timer = 0;
	Input* userIn = new Input();
	Map* map = new Map();
	PlayerData* player = new PlayerData();
	time_t lastUpdate;
	time(&lastUpdate);

	//Find the start position, set the players coordinates.
	for (int y = 0; y < MapSizeY; y++) {
		for (int x = 0; x < MapSizeX; x++) {
			if (map->data[y][x] == 'P') {
				player->posX = x;
				player->posY = y;
			}
		}
	}

	DrawMap(map, 0);

	thread inputThread(InputLoop, userIn);

	while (userIn->IsDone() == false)
	{
		time_t currentTime;
		time(&currentTime);
		double delay = 0.1f;
		if (difftime(currentTime, lastUpdate) > delay)
		{
			++timer;
			time(&lastUpdate);
			Update(userIn, player, map);
			DrawMap(map, timer);
		}
	}

	inputThread.join();

	system("pause");
}